﻿using System;

[Serializable]
public class Spotlight
{
    public string name;
    public string bannerURL;
    public string description;
}

[Serializable]
public class Product
{
    public string name;
    public string imageURL;
    public string description;
}

[Serializable]
public class Cash
{
    public string title;
    public string bannerURL;
    public string description;
}

[Serializable]
public class Page
{
    public Spotlight[] spotlight;
    public Product[]   products;
    public Cash        cash;
 }