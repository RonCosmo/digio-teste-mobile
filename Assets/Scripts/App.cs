﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;

public class App : MonoBehaviour
{
    #region Variables
    [Header("GUI Objects")]
    public GameObject       SpotlightPanel;
    public GameObject       ProductsPanel;
    public GameObject       ProductsTitle;
    public GameObject       CashTitle;
    public Image            CashImage;
    public Text             MsgInfo;

    [Header("Main Objects")]
    [Tooltip("Spotlight")]
    public RectTransform    SpotlightContent;
    [Tooltip("Products")]
    public RectTransform    ProductsContent;

    [Header("Debugging")]
    [Tooltip("Main Object")]
    public Page             PageObject;
    [Multiline]
    [Tooltip("Result from website")]
    public string           WEBResult;
    [Tooltip("Last error message")]
    public string           LError;
    [Tooltip("An error has occurred")]
    public bool             BError;
    #endregion

    private void Start()
    {
#if UNITY_EDITOR
        string URL = "http://149.56.238.42/digio/products.json";
#else
        string URL = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products";
#endif
        Application.runInBackground = true;

        InfoMsg("...Conectando ao servidor...");

        StartCoroutine(JSONDownload(URL));
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    public bool BuildLayout(string jsonFromWEB)
    {
        if (JSONSimplestCheck(jsonFromWEB))
        {
            PageObject = JSONParse(jsonFromWEB);

            if (PageObject != null)
            {
                InfoMsg("");

                if (PageObject.spotlight != null && PageObject.spotlight.Length > 0)
                    StartCoroutine(SpotlightDownload());

                if (PageObject.products  != null && PageObject.products.Length  > 0)
                    StartCoroutine(ProductsDownload());

                if (PageObject.cash      != null && !string.IsNullOrEmpty(PageObject.cash.title))
                    StartCoroutine(CashDownload());

                return true;
            }
            else
            {
                InfoMsg("Problemas com o retorno do servidor\nTente novamente mais tarde....");
            }
        }
        else
        {
            InfoMsg("Problemas com o retorno do servidor\nTente novamente mais tarde....");
        }

        return false;
    }

    public Page JSONParse(string json)
    {
        try
        {
            return JsonUtility.FromJson<Page>(json);
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
            return null;
        }
    }

    public bool JSONSimplestCheck(string json) // Very simple check!
    {
        if (json.Length > 10)
        {
            int numberOfOBraces  = json.Length - json.Replace("{", "").Length;
            int numberOfCBraces  = json.Length - json.Replace("}", "").Length;
            int numberOfOBracket = json.Length - json.Replace("[", "").Length;
            int numberOfCBracket = json.Length - json.Replace("]", "").Length;
            int numberOfCollons  = json.Length - json.Replace(":", "").Length;

            return (
                      numberOfCBraces  >  0                &&
                      numberOfOBraces  == numberOfCBraces  &&
                      numberOfOBracket == numberOfCBracket &&
                      numberOfCollons  >= numberOfOBraces
                   );
        }

        return false;
    }

    public bool InfoMsg(string description)
    {
        if (string.IsNullOrEmpty(description))
        {
            if (MsgInfo != null)
                MsgInfo.gameObject.SetActive(false);
            return false;
        }
        else
        {
            if (MsgInfo != null)
            {
                MsgInfo.gameObject.SetActive(true);
                MsgInfo.text = description;
            }
            return true;
        }
    }

    private IEnumerator CashDownload()
    {
        using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(PageObject.cash.bannerURL))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                print("ERROR [" + www.error + ") => " + PageObject.cash.title);
            }
            else
            {
                Texture2D texture = DownloadHandlerTexture.GetContent(www);
                CashImage.sprite  = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);

                CashImage.gameObject.SetActive(true);
                CashTitle.SetActive(true);
            }
        }
    }

    private IEnumerator ProductsDownload()
    {
        int PanelCount = 0;

        for (int pos = 0; pos < PageObject.products.Length; pos++)
        {
            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(PageObject.products[pos].imageURL))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    print("ERROR [" + www.error + ") => " + PageObject.products[pos].name);
                }
                else
                {
                    PanelCount++;

                    Texture2D  texture     = DownloadHandlerTexture.GetContent(www);
                    GameObject imageObject = new GameObject("[Product-Image] " + PageObject.products[pos].name);

                    imageObject.transform.SetParent(ProductsContent);
                    imageObject.AddComponent<Image>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                }

                ProductsPanel.GetComponent<DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap>().Setup(true);
            }
        }

        if (PanelCount > 1)
        {
            ProductsPanel.GetComponent<DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap>().startingPanel = 1;
            ProductsPanel.GetComponent<DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap>().Setup(true);
        }

        ProductsTitle.SetActive(PanelCount > 0);
    }

    private IEnumerator SpotlightDownload()
    {
        for (int pos = 0; pos < PageObject.spotlight.Length; pos++)
        {
            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(PageObject.spotlight[pos].bannerURL))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    print("ERROR [" + www.error + ") => " + PageObject.spotlight[pos].name);
                }
                else
                {
                    Texture2D  texture     = DownloadHandlerTexture.GetContent(www);
                    GameObject imageObject = new GameObject("[Spotlight-Banner] " + PageObject.spotlight[pos].name);

                    imageObject.transform.SetParent(SpotlightContent);
                    imageObject.AddComponent<Image>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                }

            }

            SpotlightPanel.GetComponent<DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap>().Setup(true);
        }
    }

    private IEnumerator JSONDownload(string URL)
    {
        UnityWebRequest www = UnityWebRequest.Get(URL);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            BError = true;
            LError = www.error;
            print("ERROR [" + www.error + ") => " + PageObject.cash.title);
        }
        else
        {
            BError = false;
            LError = "";
            WEBResult = www.downloadHandler.text;
        }

        if (BError)
            InfoMsg("Não foi possível conectar ao servidor.\nTente novamente mais tarde...\n<color=grey>[" + LError + "]</color>");
        else
            BuildLayout(WEBResult);
    }
}
