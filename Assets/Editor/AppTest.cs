﻿using UnityEngine;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace Tests
{
    public class AppTest
    {
        #region InfoMsg_Test
        [Test]
        [TestCase(false, "")]
        [TestCase(false, "" + "")]
        [TestCase(true , "" + "warning")]
        [TestCase(true , "<color=gray>Hi!</color>")]
        [TestCase(true , "Frase em português e com acento")]
        [TestCase(true , "----")]
        #endregion
        public void InfoMsg_Test
            (
            bool expectedResult,
            string description
            )
        {
            Assert.That(new GameObject().AddComponent<App>().GetComponent<App>().InfoMsg(description),
                Is.EqualTo(expectedResult));
        }

        #region JSONSimplestCheck_Test
        [Test]
        [TestCase(false, "")]
        [TestCase(false, "" + "")]
        [TestCase(false, "" + "{}")]
        [TestCase(false, "<color=gray>Hi!</color>")]
        [TestCase(false, "Frase em português e com acento")]
        [TestCase(false, "----")]
        [TestCase(true, "{ \"nome\": \"Pedro\",\"altura\": 1.90}")]
        [TestCase(true, "{\"Simple\": [{\"name1\":\"Anna\"},{\"name2\":\"Bella\"}]}")]
        #endregion
        public void JSONSimplestCheck_Test
            (
            bool expectedResult,
            string json
            )
        {
            Assert.That(new GameObject().AddComponent<App>().GetComponent<App>().JSONSimplestCheck(json),
                Is.EqualTo(expectedResult));
        }

        #region JSONParse_Test
        [Test]
        [TestCase(null, "")]
        [TestCase(null, "" + "")]
        [TestCase(null, "<color=gray>Hi!</color>")]
        [TestCase(null, "Frase em português e com acento")]
        [TestCase(null, "----")]
        [TestCase(true, "" + "{}")]
        [TestCase(true, "{ \"nome\": \"Pedro\",\"altura\": 1.90}")]
        [TestCase(true, "{\"Simple\": [{\"name1\":\"Anna\"},{\"name2\":\"Bella\"}]}")]
        #endregion
        public void JSONParse_Test
            (
            bool expectedResult,
            string json
            )
        {
            LogAssert.ignoreFailingMessages = true;

            if (expectedResult == true)
                Assert.AreNotEqual(new GameObject().AddComponent<App>().GetComponent<App>().JSONParse(json), null);
            else
                Assert.AreNotEqual(new GameObject().AddComponent<App>().GetComponent<App>().JSONParse(json), false);
        }

        #region BuildLayout_Test
        [Test]
        [TestCase(false, "")]
        [TestCase(false, "" + "")]
        [TestCase(false, "<color=gray>Hi!</color>")]
        [TestCase(false, "Frase em português e com acento")]
        [TestCase(false, "----")]
        [TestCase(false, "" + "{}")]
        [TestCase(true, "{ \"nome\": \"Pedro\",\"altura\": 1.90}")]
        [TestCase(true, "{\"Simple\": [{\"name1\":\"Anna\"},{\"name2\":\"Bella\"}]}")]
        #endregion
        public void BuildLayout_Test
            (
            bool expectedResult,
            string json
            )
        {
            LogAssert.ignoreFailingMessages = true;

            Assert.That(new GameObject().AddComponent<App>().GetComponent<App>().BuildLayout(json),
                Is.EqualTo(expectedResult));
        }
    }
}
